#!/bin/bash

cd "$HOME"

if [[ "$1" = "backup" ]]; then
	echo "backing up"
	gpg --export-secret-subkeys "$(cat .password-store/.gpg-id)" >pwdstore_secret.gpg
	tar cvf pass.tar .password-store pwdstore_secret.gpg
	gpg -c pass.tar
	shred -u pwdstore_secret.gpg pass.tar
elif [[ "$1" = "restore" ]]; then echo "restoring"
	gpg -d pass.tar.gpg | tar xvf -
	gpg --import pwdstore_secret.gpg
	shred -u pwdstore_secret.gpg
fi
