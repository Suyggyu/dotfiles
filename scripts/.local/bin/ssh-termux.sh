#!/usr/bin/env sh

user="user"
ports=()

## i2pd
#ports+=(4444 7070)
## noVNC
#ports+=(6008)
# vnc
ports+=(5901)
## ipfs
#ports+=(5001 8080)
## transmission
ports+=(9091)

if [ "$1" = "local" ]; then
  port=8022
  url="127.0.0.1"
else
  # fetch ngrok forwarded url
  api_key="$(pass api/ngrok)"
  public_url="$(curl -s -X GET -H "Authorization: Bearer $api_key" -H "Ngrok-Version: 2" https://api.ngrok.com/tunnels | jq '.tunnels[0].public_url')"
  public_url="${public_url#*//}"
  public_url="${public_url%%\"}"

  port="${public_url#*:}"
  url="${public_url%:*}"
fi

# generate ports_str
ports_str=""
for i in ${ports[@]}; do ports_str="$ports_str -L $i:localhost:$i"; done

case "$1" in
  "sftp")
    sftp -P $port $user@$url
    ;;
  *)
    ssh $ports_str -t -p $port $user@$url -- $@
    [ $? -eq 0 ] || ssh $ports_str -t -p $port $user@192.168.91.137
    ;;
esac
